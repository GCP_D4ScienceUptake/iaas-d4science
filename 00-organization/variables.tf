variable "billing_account_id" {
  description = "D4Science Billing account id."
  type        = string
}

variable "groups" {
  description = "Group names to grant organization-level permissions."
  type        = map(string)
  default     = {}
}

variable "organization" {
  description = "Organization details."
  type = object({
    domain = string
    id     = number
  })
}

variable "prefix" {
  description = "Prefix for terraform resources"
  type        = string
  default     = null
}
variable "outputs_location" {
  description = "Assets path location relative to the module"
  type        = string
  default     = "../assets"
}

variable "labels" {
  description = "Labels to add to the resources"
  type        = map(string)
  default     = {}
}
