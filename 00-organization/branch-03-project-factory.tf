##-------------------------------------------------------------------------------
## d4science/Prod Folder
##-------------------------------------------------------------------------------
module "d4science-prod-folder" {
  source        = "../assets/modules-fabric/v26/folder"
  parent        = "organizations/${var.organization.id}"
  name          = "Prod"
  folder_create = true

  #https://github.com/GoogleCloudPlatform/cloud-foundation-fabric/tree/master/blueprints/factories/project-factory
  iam = {
    #Required for Project Factory
    "roles/logging.admin"                  = [module.d4science-project-factory-prod-tfsa.iam_email]
    "roles/owner"                          = [module.d4science-project-factory-prod-tfsa.iam_email]
    "roles/resourcemanager.folderAdmin"    = [module.d4science-project-factory-prod-tfsa.iam_email]
    "roles/resourcemanager.projectCreator" = [module.d4science-project-factory-prod-tfsa.iam_email]

    #To enable Shared VPC Service projects
    "roles/compute.xpnAdmin" = [module.d4science-networking-tfsa.iam_email]
  }
}

##-------------------------------------------------------------------------------
## d4science/Test Folder
##-------------------------------------------------------------------------------
module "d4science-test-folder" {
  source        = "../assets/modules-fabric/v26/folder"
  parent        = "organizations/${var.organization.id}"
  name          = "Test"
  folder_create = true

  #https://github.com/GoogleCloudPlatform/cloud-foundation-fabric/tree/master/blueprints/factories/project-factory
  iam = {
    #Required for Project Factory
    "roles/logging.admin"                  = [module.d4science-project-factory-test-tfsa.iam_email]
    "roles/owner"                          = [module.d4science-project-factory-test-tfsa.iam_email]
    "roles/resourcemanager.folderAdmin"    = [module.d4science-project-factory-test-tfsa.iam_email]
    "roles/resourcemanager.projectCreator" = [module.d4science-project-factory-test-tfsa.iam_email]

    #To enable Shared VPC Service projects
    "roles/compute.xpnAdmin" = [module.d4science-networking-tfsa.iam_email]
  }
}

##-------------------------------------------------------------------------------
## 03 - Project Factory - TF SA, impersonated to apply Terraform config
##-------------------------------------------------------------------------------

module "d4science-project-factory-test-tfsa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.d4science-seed-project.project_id
  name       = "d4science-test-tfprj-sa"
  prefix     = var.prefix

  iam = {
    "roles/iam.serviceAccountTokenCreator" = ["group:foundationreply@d4science.org"]
    #Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  }

  iam_billing_roles = {
    "${var.billing_account_id}" = ["roles/billing.admin"]
  }
}

module "d4science-project-factory-prod-tfsa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.d4science-seed-project.project_id
  name       = "d4science-prod-tfprj-sa"
  prefix     = var.prefix

  iam = {
    "roles/iam.serviceAccountTokenCreator" = ["group:foundationreply@d4science.org"]
    #Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  }

  iam_billing_roles = {
    "${var.billing_account_id}" = ["roles/billing.admin"]
  }
}

##############
##############
##############

##-------------------------------------------------------------------------------
## 03 - Project Factory - TF Bucket, store Terraform state
##-------------------------------------------------------------------------------

module "d4science-project-factory-test-tfbucket" {
  source = "../assets/modules-fabric/v26/gcs"

  name       = "d4science-test-ew8-foundation-tfprj-bkt"
  project_id = module.d4science-seed-project.project_id
  prefix     = var.prefix

  versioning = true
  iam = {
    "roles/storage.objectAdmin" = [module.d4science-project-factory-test-tfsa.iam_email]
  }

  location      = "EUROPE-WEST8"
  storage_class = "STANDARD"

  labels = var.labels
}



module "d4science-project-factory-prod-tfbucket" {
  source = "../assets/modules-fabric/v26/gcs"

  name       = "d4science-prod-ew8-foundation-tfprj-bkt"
  project_id = module.d4science-seed-project.project_id
  prefix     = var.prefix

  versioning = true
  iam = {
    "roles/storage.objectAdmin" = [module.d4science-project-factory-prod-tfsa.iam_email]
  }

  location      = "EUROPE-WEST8"
  storage_class = "STANDARD"

  labels = var.labels
}
