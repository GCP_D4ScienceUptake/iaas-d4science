# tfdoc:file:description Terraform tfvars output files.

locals {
  tfvars = {

    billing_account_id = var.billing_account_id
    organization       = var.organization
    prefix             = var.prefix
    labels             = var.labels
    groups             = var.groups

    # Global variables

    seed-project = {
      project_id     = module.d4science-seed-project.project_id
      project_number = module.d4science-seed-project.number
    }

    service_accounts = {
      networking           = module.d4science-networking-tfsa.iam_email
      security             = module.d4science-security-tfsa.iam_email
      project_factory_test = module.d4science-project-factory-test-tfsa.iam_email
      project_factory_prod = module.d4science-project-factory-prod-tfsa.iam_email
    }

    monitoring = {
      bucket-sink-test = module.common-test-organization-monitoring-bucket
      bucket-sink-prod = module.common-prod-organization-monitoring-bucket
      project_id       = module.common-organization-monitoring-project.project_id
      channels = {
        "budget-alerting" = google_monitoring_notification_channel.budget_alerting.id
      }
    }

    # Folders
    folders = {
      networking = {
        id   = module.d4science-networking-folder.id
        name = module.d4science-networking-folder.name
      }
      prod = {
        id   = module.d4science-prod-folder.id
        name = module.d4science-prod-folder.name
      }
      test = {
        id   = module.d4science-test-folder.id
        name = module.d4science-test-folder.name
      }
    }
  }
}

resource "local_file" "tfvars" {
  for_each        = var.outputs_location == null ? {} : { 1 = 1 }
  file_permission = "0644"
  filename        = "${path.module}/${var.outputs_location}/tfvars/00-organization.auto.tfvars.json"
  content         = jsonencode(local.tfvars)
}

output "tfvars" {
  sensitive = true
  value     = local.tfvars
}
