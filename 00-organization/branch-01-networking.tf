##-------------------------------------------------------------------------------
## d4science - NETWORKING Folder
##-------------------------------------------------------------------------------
module "d4science-networking-folder" {
  source        = "../assets/modules-fabric/v26/folder"
  parent        = "organizations/${var.organization.id}"
  name          = "Networking"
  folder_create = true

  iam = {
    "roles/owner" = [
      module.d4science-networking-tfsa.iam_email,
      "group:foundationreply@d4science.org"
    ]
    "roles/compute.xpnAdmin" = [module.d4science-networking-tfsa.iam_email] #to enable shared VPC
    #to enable 01-networking create the hub
    "roles/resourcemanager.projectCreator" = [module.d4science-networking-tfsa.iam_email]
  }

  #  iam_additive = {
  ##        "roles/resourcemanager.projectCreator" = [module.common-terraform-sa.iam_email] # required to create project within this folder
  #    "roles/resourcemanager.projectCreator" = [] # required to create project within this folder
  #
  #  }

}


##-------------------------------------------------------------------------------
## 01 - Networking - TF SA, impersonated to apply Terraform config
##-------------------------------------------------------------------------------
module "d4science-networking-tfsa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.d4science-seed-project.project_id
  name       = "d4science-com-tfnet-sa"
  prefix     = var.prefix

  iam = {
    "roles/iam.serviceAccountTokenCreator" = ["group:foundationreply@d4science.org"]
    #Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  }

  iam_billing_roles = {
    "${var.billing_account_id}" = ["roles/billing.user"]
  }
}


#
##-------------------------------------------------------------------------------
## 01 - Networking - TF Bucket, store Terraform state
##-------------------------------------------------------------------------------
module "d4science-networking-tfbucket" {
  source = "../assets/modules-fabric/v26/gcs"

  name       = "d4science-com-ew8-foundation-tfnet-bkt"
  project_id = module.d4science-seed-project.project_id
  prefix     = var.prefix

  versioning = true
  iam = {
    "roles/storage.objectAdmin" = [module.d4science-networking-tfsa.iam_email]
  }

  location      = "EUROPE-WEST8"
  storage_class = "STANDARD"

  labels = var.labels
}

