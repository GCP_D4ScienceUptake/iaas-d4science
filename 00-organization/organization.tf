##-------------------------------------------------------------------------------
## BCC_PAY - Organization
##-------------------------------------------------------------------------------
module "organization" {
  source          = "../assets/modules-fabric/v26/organization"
  organization_id = "organizations/${var.organization.id}"

  custom_roles = {
    # Allow gke policy creation. Assign this to gke sa
    "d4science_org_glb_gkemanagefwrules_role" = [
      "compute.networks.updatePolicy",
      "compute.firewalls.list",
      "compute.firewalls.get",
      "compute.firewalls.create",
      "compute.firewalls.update",
      "compute.firewalls.delete"
    ]
  }
}
