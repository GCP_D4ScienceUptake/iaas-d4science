terraform {
  backend "gcs" {
    bucket                      = "d4science-com-ew8-foundation-tforg-bkt"
    impersonate_service_account = "d4science-com-tforg-sa@d4science-com-automation-prj.iam.gserviceaccount.com"
  }
}

provider "google" {
  impersonate_service_account = "d4science-com-tforg-sa@d4science-com-automation-prj.iam.gserviceaccount.com"
}

provider "google-beta" {
  impersonate_service_account = "d4science-com-tforg-sa@d4science-com-automation-prj.iam.gserviceaccount.com"
}

provider "google" {
  alias = "no-impersonate"
}