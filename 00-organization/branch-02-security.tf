##-------------------------------------------------------------------------------
## 02 - Security - TF SA, impersonated to apply Terraform config
##-------------------------------------------------------------------------------
module "d4science-security-tfsa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.d4science-seed-project.project_id
  name       = "d4science-com-tfsec-sa"
  prefix     = var.prefix

  iam = {
    "roles/iam.serviceAccountTokenCreator" = ["group:foundationreply@d4science.org"]
    #Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  }

  iam_organization_roles = {
    "${var.organization.id}" = ["roles/resourcemanager.organizationAdmin"]
  }
}


#
##-------------------------------------------------------------------------------
## 01 - Networking - TF Bucket, store Terraform state
##-------------------------------------------------------------------------------
module "d4science-security-tfbucket" {
  source = "../assets/modules-fabric/v26/gcs"

  name       = "d4science-com-ew8-foundation-tfsec-bkt"
  project_id = module.d4science-seed-project.project_id
  prefix     = var.prefix

  versioning = true
  iam = {
    "roles/storage.objectAdmin" = [module.d4science-security-tfsa.iam_email]
  }

  location      = "EUROPE-WEST8"
  storage_class = "STANDARD"

  labels = var.labels
}
