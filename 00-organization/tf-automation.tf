
module "d4science-seed-project" {
  source          = "../assets/modules-fabric/v26/project"
  name            = "d4science-com-automation-prj"
  project_create  = true
  billing_account = var.billing_account_id
  parent          = "organizations/${var.organization.id}"
  services = [
    "cloudresourcemanager.googleapis.com", #required for SA impersonification
    "serviceusage.googleapis.com",         #required for SA impersonification
    "iam.googleapis.com",                  #required for IAM and SA impersonification
    "cloudbilling.googleapis.com",         #required for project creation
    "accesscontextmanager.googleapis.com", #required for VPC SC
    "logging.googleapis.com",              #required for sink creation
    "servicenetworking.googleapis.com",    #required for Private Service Access
    "pubsub.googleapis.com",               #required for PubSub
    "monitoring.googleapis.com",           #required for Monitoring
    "billingbudgets.googleapis.com",       #required for Billing budgets
    "orgpolicy.googleapis.com"             #required for Organizational policies
  ]

  iam = {
    "roles/owner" = [
      module.d4science-organization-tfsa.iam_email,
    ]
  }

  group_iam = {
    "foundationreply@d4science.org" = [
      "roles/owner"
    ]
  }

  labels = var.labels
}
##-------------------------------------------------------------------------------
## Terraform bucket for storing TFSTATE
##-------------------------------------------------------------------------------
module "d4science-organization-tfbucket" {
  source = "../assets/modules-fabric/v26/gcs"

  name       = "d4science-com-ew8-foundation-tforg-bkt"
  project_id = module.d4science-seed-project.project_id

  prefix = var.prefix

  versioning = true

  iam = {
    "roles/storage.objectAdmin" = [module.d4science-organization-tfsa.iam_email]
  }

  # Maintain last 3 versions (1 live plus 2).
  lifecycle_rules = {
    limit-versions = {
      action = {
        type = "Delete"
      }
      condition = {
        num_newer_versions = 3
        with_state         = "ARCHIVED"
      }
    }
  }

  location      = "EUROPE-WEST8"
  storage_class = "STANDARD"

  labels = var.labels
}


##-------------------------------------------------------------------------------
## TF SA, impersonated to apply Terraform config
##-------------------------------------------------------------------------------
module "d4science-organization-tfsa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.d4science-seed-project.project_id
  name       = "d4science-com-tforg-sa"
  prefix     = var.prefix

  iam = {
    "roles/iam.serviceAccountTokenCreator" = ["group:foundationreply@d4science.org"]
    #Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  }
}
