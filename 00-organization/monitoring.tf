module "common-organization-monitoring-project" {
  source          = "../assets/modules-fabric/v26/project"
  name            = "d4science-com-monitoring-prj"
  project_create  = true
  parent          = "organizations/${var.organization.id}"
  billing_account = var.billing_account_id
  services = [
    "cloudresourcemanager.googleapis.com", #required for SA impersonification
    "iam.googleapis.com",                  #required for IAM and SA impersonification
    "secretmanager.googleapis.com",        #required for secrets
    "monitoring.googleapis.com"            #required for monitoring
  ]

  iam = {
    "roles/owner" = [
      module.d4science-organization-tfsa.iam_email,
    ]
    "roles/resourcemanager.projectIamAdmin" = [
      module.d4science-project-factory-prod-tfsa.iam_email,
      module.d4science-project-factory-test-tfsa.iam_email
    ],
    "roles/monitoring.notificationChannelViewer" = [
      module.d4science-project-factory-test-tfsa.iam_email,
      module.d4science-project-factory-prod-tfsa.iam_email
    ]
  }

  group_iam = {
    "foundationreply@d4science.org" = [
      "roles/owner"
    ]
  }
}

module "common-test-organization-monitoring-bucket" {
  source      = "../assets/modules-fabric/v26/logging-bucket"
  parent_type = "project"
  parent      = module.common-organization-monitoring-project.project_id
  id          = "d4science-test-monitoring-sink-bucket"
  location    = "europe-west8"
  description = "Terraform-managed."
}

module "common-prod-organization-monitoring-bucket" {
  source      = "../assets/modules-fabric/v26/logging-bucket"
  parent_type = "project"
  parent      = module.common-organization-monitoring-project.project_id
  id          = "d4science-prod-monitoring-sink-bucket"
  location    = "europe-west4"
  description = "Terraform-managed."
}

module "common-organization-monitoring-sa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.common-organization-monitoring-project.project_id
  name       = "d4science-com-monitoring-sa"
  prefix     = var.prefix

  iam_organization_roles = {
    "${var.organization.id}" = ["roles/monitoring.viewer"]
  }
}

resource "google_monitoring_notification_channel" "budget_alerting" {
  display_name = "Budget alerting"
  type         = "email"
  project      = module.common-organization-monitoring-project.project_id
  labels = {
    email_address = "budget@d4science.org"
  }
  user_labels = {}
}