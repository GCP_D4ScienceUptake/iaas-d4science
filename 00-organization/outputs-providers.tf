# tfdoc:file:description Providers output files.

locals {
  providers = {
    "00-organization" = {
      bucket          = module.d4science-organization-tfbucket.name
      service_account = module.d4science-organization-tfsa.email
    }
    "01-networking" = {
      bucket          = module.d4science-networking-tfbucket.name
      service_account = module.d4science-networking-tfsa.email
    }
    "02-security" = {
      bucket          = module.d4science-security-tfbucket.name
      service_account = module.d4science-security-tfsa.email
    }
    "03-project-factory-test" = {
      bucket          = module.d4science-project-factory-test-tfbucket.name
      service_account = module.d4science-project-factory-test-tfsa.email
    }
    "03-project-factory-prod" = {
      bucket          = module.d4science-project-factory-prod-tfbucket.name
      service_account = module.d4science-project-factory-prod-tfsa.email
    }
  }
}

resource "local_file" "other_providers" {
  for_each = var.outputs_location == null ? {} : local.providers

  file_permission = "0644"
  filename        = "${path.module}/${var.outputs_location}/providers/${each.key}.providers.tf"
  content = templatefile("${path.module}/../assets/providers.tpl", {
    bucket = each.value.bucket
    sa     = each.value.service_account
    prefix = try(each.value.prefix, null)
  })
}
