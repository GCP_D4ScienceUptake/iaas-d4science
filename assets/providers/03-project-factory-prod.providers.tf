terraform {
  backend "gcs" {
    bucket                      = "d4science-prod-ew8-foundation-tfprj-bkt"
    impersonate_service_account = "d4science-prod-tfprj-sa@d4science-com-automation-prj.iam.gserviceaccount.com"
  }

  required_version = "~> 1.6.5"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.84.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "4.84.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.4.0"
    }
  }
}

provider "google" {
  impersonate_service_account = "d4science-prod-tfprj-sa@d4science-com-automation-prj.iam.gserviceaccount.com"
}

provider "google-beta" {
  impersonate_service_account = "d4science-prod-tfprj-sa@d4science-com-automation-prj.iam.gserviceaccount.com"
}

provider "google" {
  alias = "no-impersonate"
}

provider "local" {}