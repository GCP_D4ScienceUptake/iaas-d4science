terraform {
  backend "gcs" {
    bucket                      = "${bucket}"
    %{~ if sa != null ~}
    impersonate_service_account = "${sa}"
    %{~ endif ~}
    %{~ if prefix != null ~}
    prefix                      = "${prefix}"
    %{~ endif ~}
  }

  required_version = "~> 1.6.5"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.84.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "4.84.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.4.0"
    }
  }
}

%{ if sa != null ~}
provider "google" {
  impersonate_service_account = "${sa}"
}

provider "google-beta" {
  impersonate_service_account = "${sa}"
}

provider "google" {
  alias = "no-impersonate"
}
%{~ endif }

provider "local" {}