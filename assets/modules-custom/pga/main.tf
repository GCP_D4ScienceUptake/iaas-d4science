#-------------------------------------------------------------------------------
# DNS Zones for PRIVATE GOOGLE ACCESS
#-------------------------------------------------------------------------------
/* !!!!! README !!!!!
 * We use restricted.googleapis.com and additional domains linked to restricted APIS.
 * Private Google Apis (private.googleapis.com) has been left here for future use, if needed.
 */
locals {
  private_ips    = ["199.36.153.8", "199.36.153.9", "199.36.153.10", "199.36.153.11"]
  restricted_ips = ["199.36.153.4", "199.36.153.5", "199.36.153.6", "199.36.153.7"]
  ips            = var.config.private ? local.private_ips : local.restricted_ips
  name           = var.config.private ? "private" : "restricted"

  fixed_domains = {
    accounts   = ["accounts.google.com"]
    appengine  = ["appengine.google.com"]
    appspot    = ["*.appspot.com"]
    artifact   = ["*.gcr.io", "*.pkg.dev"]
    functions  = ["*.cloudfunctions.net"]
    proxy      = ["*.cloudproxy.app"]
    run        = ["*.run.app"]
    composer   = ["*.composer.cloud.google.com", "*.composer.googleusercontent.com"]
    datafusion = ["*.datafusion.cloud.google.com", "*.datafusion.googleusercontent.com"]
    dataproc   = ["*.dataproc.cloud.google.com", "dataproc.cloud.google.com", "*.dataproc.googleusercontent.com", "dataproc.googleusercontent.com"]
    download   = ["dl.google.com"]
    ad         = ["*.googleadapis.com"]
    gstatic    = ["*.gstatic.com"]
    lts        = ["*.ltsapis.goog"]
    notebooks  = ["*.notebooks.cloud.google.com", "*.notebooks.googleusercontent.com"]
    packages   = ["packages.cloud.google.com"]
    pki        = ["*.pki.goog"]
    source     = ["source.developers.google.com"]
  }

  enabled_fixed_domain = merge([
    for k, v in var.domains : {
      for i, d in local.fixed_domains[k] :
      "${k}-${i}" => d
    } if v
  ]...)

  enabled_custom_domain = merge([
    for k, v in var.custom_domains : {
      for i, d in v :
      "${k}-${i}" => d
    }
  ]...)

  enabled_domain = merge(local.enabled_fixed_domain, local.enabled_custom_domain)

}

#-------------------------------------
# PRIVATE.GOOGLEAPIS.COM
#-------------------------------------
# DNS zone for PGA: *.googleapis.com.
module "googleapis_zone" {
  source          = "../../modules-fabric/v26/dns"
  project_id      = var.project_id
  name            = "${var.name}-pga-${local.name}-apis"
  zone_config = {
    domain          = "googleapis.com."
    private = {
      client_networks = var.networks
    }
  }
  recordsets = {
    "A ${local.name}.googleapis.com." = { ttl = 300, records = local.ips }
    "CNAME *.googleapis.com."         = { ttl = 300, records = ["${local.name}.googleapis.com."] }
  }
}

# DNS zone for others APIs
# All others api (different from *.googleapis.com)
module "pga_apis_zone" {
  for_each        = local.enabled_domain
  source          = "../../modules-fabric/v26/dns"
  project_id      = var.project_id
  name            = "${var.name}-pga-${local.name}-api-${each.key}"
  zone_config = {
    domain          = "${trimprefix(each.value, "*.")}."
    private = {
      client_networks = var.networks
    }
  }
  recordsets = {
    # We are adding wildcard even if not needed, as per documentation.
    "A ${trimprefix(each.value, "*.")}."       = { ttl = 300, records = local.ips }
    "CNAME *.${trimprefix(each.value, "*.")}." = { ttl = 300, records = ["${trimprefix(each.value, "*.")}."] }
  }
}

resource "google_service_networking_peered_dns_domain" "apis-dns-peering" {
  for_each   = can(var.config.value["service_networking_network"]) ? local.enabled_domain : {}
  project    = var.project_id
  network    = var.config.service_networking_network
  name       = "${replace(trimprefix(each.value, "*."), ".", "-")}-domain"
  dns_suffix = "*.${trimprefix(each.value, "*.")}."
}

resource "google_service_networking_peered_dns_domain" "googleapis-dns-peering" {
  count      = can(var.config.value["service_networking_network"]) ? 1 : 0
  project    = var.project_id
  name       = "googleapis-domain"
  network    = var.config.service_networking_network
  dns_suffix = "googleapis."
}
