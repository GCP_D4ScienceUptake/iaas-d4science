variable "domains" {
  description = "Declare which domains to redirect to PGA"
  type = object({
    accounts   = optional(bool, false)
    appengine  = optional(bool, false)
    appspot    = optional(bool, false)
    artifact   = optional(bool, false)
    functions  = optional(bool, false)
    proxy      = optional(bool, false)
    run        = optional(bool, false)
    composer   = optional(bool, false)
    datafusion = optional(bool, false)
    dataproc   = optional(bool, false)
    download   = optional(bool, false)
    ad         = optional(bool, false)
    gstatic    = optional(bool, false)
    lts        = optional(bool, false)
    notebooks  = optional(bool, false)
    packages   = optional(bool, false)
    pki        = optional(bool, false)
    source     = optional(bool, false)
  })
  default = {}
}

variable "custom_domains" {
  type        = map(list(string))
  description = "Optional map {NAME}=>[{domain1},{domain2},...]"
  default     = {}
}

variable "config" {
  type = object({
    private                    = optional(bool, false)
    restricted                 = optional(bool, false)
    service_networking_network = optional(string)
  })
  description = "(optional) describe your variable"

  validation {
    condition     = var.config.private != var.config.restricted
    error_message = "One and only one between private and restricted must be set to true"
  }
}

variable "project_id" {
  type        = string
  description = "DNS zone project id"
}

variable "name" {
  type        = string
  description = "Name for the created resources"
}

variable "networks" {
  type        = list(string)
  description = "List of networks self-links for DNS zone"
}
