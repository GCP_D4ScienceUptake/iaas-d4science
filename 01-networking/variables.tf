variable "outputs_location" {
  description = "Assets path location relative to the module"
  type        = string
  default     = "../assets"
}

variable "service_projects" {
  description = "A map of lists reporting projects to attach to a shared VPC of each environment"
  type        = map(list(string))
  default = {
    networking : [],
    test : [],
    prod : []
  }
}

variable "psa_ranges" {
  description = "A map of maps of address ranges for private service access"
  type        = map(map(string))
  default = {
    test : {}
    prod : {}
  }
}