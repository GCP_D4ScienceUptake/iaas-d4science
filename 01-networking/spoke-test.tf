#----------------------------------
# Project
#----------------------------------
module "d4science-networking-spoke-test-project" {
  source = "../assets/modules-fabric/v26/project"

  name            = "d4science-test-spoke-prj"
  prefix          = local.prefix
  billing_account = local.billing_account_id
  parent          = local.folders.networking.id
  labels          = local.labels

  auto_create_network = false
  project_create      = true

  # Enable Shared VPC
  shared_vpc_host_config = {
    enabled = true
    # First run the next line must be commented, after you execute step 3 you can uncomment it
    # because you first need to create the service projects
    service_projects = var.service_projects["test"]
  }

  services = [
    "vpcaccess.googleapis.com", # to enable VPC Access Connector
    "container.googleapis.com",
    "certificatemanager.googleapis.com", # required for certificates
    "secretmanager.googleapis.com",
    "servicenetworking.googleapis.com"
  ]

  iam = {
    "roles/owner"                 = []
    "roles/editor"                = ["serviceAccount:${module.d4science-networking-spoke-test-project.service_accounts.cloud_services}"]
    "roles/compute.securityAdmin" = [local.service_accounts.security] #to create firewall rules

    #https://github.com/GoogleCloudPlatform/cloud-foundation-fabric/tree/master/blueprints/factories/project-factory
    #Required for Project Factory
    "roles/browser"        = [local.service_accounts.project_factory_test]
    "roles/compute.viewer" = [local.service_accounts.project_factory_test]
    "roles/dns.admin"      = [local.service_accounts.project_factory_test]

    # These must be done after creating the service project
    "roles/container.hostServiceAgentUser"                                                                   = ["serviceAccount:service-804925782180@container-engine-robot.iam.gserviceaccount.com"]
    "organizations/${local.organization_vars.organization.id}/roles/d4science_org_glb_gkemanagefwrules_role" = ["serviceAccount:service-804925782180@container-engine-robot.iam.gserviceaccount.com"]
  }
}


#----------------------------------
# VPC test with subnets
#----------------------------------
module "d4science-networking-test-vpc" {
  source     = "../assets/modules-fabric/v26/net-vpc"
  project_id = module.d4science-networking-spoke-test-project.project_id
  name       = "d4science-test-vpc"
  factories_config = {
    subnets_folder = "${path.root}/data/subnets/test"
  }
  psa_config = {
    ranges = { filestore = var.psa_ranges["test"]["filestore"] }
  }
}

#----------------------------------
# Network Adresses (GLB and NAT)
#----------------------------------

module "d4science-networking-spoke-test-addresses" {
  source     = "../assets/modules-fabric/v26/net-address"
  project_id = module.d4science-networking-spoke-test-project.project_id
  external_addresses = {
    nat-ew8-00-addr-00 = { region = "europe-west8" }
  }
}


#----------------------------------
# Cloud NAT (Spoke TEST)
#----------------------------------

module "d4science-networking-spoke-test-cloudnat-ew8" {
  source     = "../assets/modules-fabric/v26/net-cloudnat"
  project_id = module.d4science-networking-spoke-test-project.project_id
  region     = "europe-west8"
  name       = "d4science-test-ew8-01-nat"
  addresses  = [module.d4science-networking-spoke-test-addresses.external_addresses["nat-ew8-00-addr-00"].self_link]

  router_network        = module.d4science-networking-test-vpc.self_link
  config_source_subnets = "LIST_OF_SUBNETWORKS"
  subnetworks = [
    {
      self_link            = module.d4science-networking-test-vpc.subnet_self_links["europe-west8/d4science-test-ew8-vre-sub"]
      config_source_ranges = ["ALL_IP_RANGES"]
      secondary_ranges     = null
    }
  ]
}

##----------------------------------
## Private Google Access (TEST)
##----------------------------------

module "d4science-networking-spoke-test-pga" {
  source     = "../assets/modules-custom/pga"
  project_id = module.d4science-networking-spoke-test-project.project_id
  name       = "d4science-test-pga"
  domains = {
    artifact = true
  }
  config = {
    private = true
  }
  networks = [
    module.d4science-networking-test-vpc.self_link
  ]
}