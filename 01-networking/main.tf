locals {
  organization_vars = jsondecode(file("../assets/tfvars/00-organization.auto.tfvars.json"))
}
locals {

  billing_account_id = local.organization_vars.billing_account_id
  organization       = local.organization_vars.organization
  prefix             = local.organization_vars.prefix
  labels             = local.organization_vars.labels
  groups             = local.organization_vars.groups

  # Global variables

  seed-project = {
    project_id     = local.organization_vars.seed-project.project_id
    project_number = local.organization_vars.seed-project.project_number
  }

  service_accounts = {
    networking           = local.organization_vars.service_accounts.networking
    security             = local.organization_vars.service_accounts.security
    project_factory_test = local.organization_vars.service_accounts.project_factory_test
    project_factory_prod = local.organization_vars.service_accounts.project_factory_prod

  }

  # Folders
  folders = {
    networking = {
      id   = local.organization_vars.folders.networking.id
      name = local.organization_vars.folders.networking.name

    }
  }
}