# tfdoc:file:description Terraform tfvars output files.

locals {
  tfvars = {
    spoke-test = {
      project_id = module.d4science-networking-spoke-test-project.project_id
      number     = module.d4science-networking-spoke-test-project.number
      network    = module.d4science-networking-test-vpc.self_link
      subnets = {
        for k, v in module.d4science-networking-test-vpc.subnet_ips :
        k => {
          self_link        = module.d4science-networking-test-vpc.subnets[k].self_link
          ip               = v,
          secondary_ranges = module.d4science-networking-test-vpc.subnet_secondary_ranges[k]
        }
      }
      nat-address = module.d4science-networking-spoke-test-addresses.external_addresses["nat-ew8-00-addr-00"].address
    }

    spoke-prod = {
      project_id = module.d4science-networking-spoke-prod-project.project_id
      number     = module.d4science-networking-spoke-prod-project.number
      network    = module.d4science-networking-prod-vpc.self_link
      subnets = {
        for k, v in module.d4science-networking-prod-vpc.subnet_ips :
        k => {
          self_link        = module.d4science-networking-prod-vpc.subnets[k].self_link
          ip               = v,
          secondary_ranges = module.d4science-networking-prod-vpc.subnet_secondary_ranges[k]
        }
      }
      nat-address = module.d4science-networking-spoke-prod-addresses.external_addresses["nat-ew4-00-addr-00"].address
    }
  }
}

resource "local_file" "tfvars" {
  for_each        = var.outputs_location == null ? {} : { 1 = 1 }
  file_permission = "0644"
  filename        = "${path.module}/${var.outputs_location}/tfvars/01-networking.auto.tfvars.json"
  content         = jsonencode(local.tfvars)
}


output "tfvars" {
  sensitive = true
  value     = local.tfvars
}
