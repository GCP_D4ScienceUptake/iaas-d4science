service_projects = {
  networking : ["d4science-spoke-prod", "d4science-spoke-test"],
  test : ["d4science-test-vre-prj", "d4science-test-script-prj"],
  prod : ["d4science-prod-vre-prj", "d4science-prod-script-prj"]
}

psa_ranges = {
  "test" : { "filestore" : "10.254.65.0/24" },
  "prod" : { "filestore" : "10.254.1.0/24" },
}