#----------------------------------
# Project
#----------------------------------
module "d4science-networking-spoke-prod-project" {
  source = "../assets/modules-fabric/v26/project"

  name            = "d4science-prod-spoke-prj"
  prefix          = local.prefix
  billing_account = local.billing_account_id
  parent          = local.folders.networking.id
  labels          = local.labels

  auto_create_network = false
  project_create      = true

  # Enable Shared VPC
  shared_vpc_host_config = {
    enabled = true
    # First run the next line must be commented, after you execute step 3 you can uncomment it
    # because you first need to create the service projects
    service_projects = var.service_projects["prod"]
  }

  services = [
    "vpcaccess.googleapis.com", # to enable VPC Access Connector
    "container.googleapis.com",
    "certificatemanager.googleapis.com", # required for certificates
    "secretmanager.googleapis.com",
    "servicenetworking.googleapis.com"
  ]

  iam = {
    "roles/owner"                 = []
    "roles/editor"                = ["serviceAccount:${module.d4science-networking-spoke-prod-project.service_accounts.cloud_services}"]
    "roles/compute.securityAdmin" = [local.service_accounts.security] #to create firewall rules

    #https://github.com/GoogleCloudPlatform/cloud-foundation-fabric/tree/master/blueprints/factories/project-factory
    #Required for Project Factory
    "roles/browser"        = [local.service_accounts.project_factory_prod]
    "roles/compute.viewer" = [local.service_accounts.project_factory_prod]
    "roles/dns.admin"      = [local.service_accounts.project_factory_prod]

    # These must be done after creating the service project
    "roles/container.hostServiceAgentUser"                                                                   = ["serviceAccount:service-783244529218@container-engine-robot.iam.gserviceaccount.com"]
    "organizations/${local.organization_vars.organization.id}/roles/d4science_org_glb_gkemanagefwrules_role" = ["serviceAccount:service-783244529218@container-engine-robot.iam.gserviceaccount.com"]
  }
}


#----------------------------------
# VPC prod with subnets
#----------------------------------
module "d4science-networking-prod-vpc" {
  source     = "../assets/modules-fabric/v26/net-vpc"
  project_id = module.d4science-networking-spoke-prod-project.project_id
  name       = "d4science-prod-vpc"
  factories_config = {
    subnets_folder = "${path.root}/data/subnets/prod"
  }
  psa_config = {
    ranges = { filestore = var.psa_ranges["prod"]["filestore"] }
  }
}

#----------------------------------
# Network Adresses (GLB and NAT)
#----------------------------------

module "d4science-networking-spoke-prod-addresses" {
  source     = "../assets/modules-fabric/v26/net-address"
  project_id = module.d4science-networking-spoke-prod-project.project_id
  external_addresses = {
    nat-ew4-00-addr-00 = { region = "europe-west4" }
  }
}


#----------------------------------
# Cloud NAT (Spoke PROD)
#----------------------------------

module "d4science-networking-spoke-prod-cloudnat-ew4" {
  source     = "../assets/modules-fabric/v26/net-cloudnat"
  project_id = module.d4science-networking-spoke-prod-project.project_id
  region     = "europe-west4"
  name       = "d4science-prod-ew4-01-nat"
  addresses  = [module.d4science-networking-spoke-prod-addresses.external_addresses["nat-ew4-00-addr-00"].self_link]

  router_network        = module.d4science-networking-prod-vpc.self_link
  config_source_subnets = "LIST_OF_SUBNETWORKS"
  subnetworks = [
    {
      self_link            = module.d4science-networking-prod-vpc.subnet_self_links["europe-west4/d4science-prod-ew4-vre-sub"]
      config_source_ranges = ["ALL_IP_RANGES"]
      secondary_ranges     = null
    }
  ]
}

##----------------------------------
## Private Google Access (PROD)
##----------------------------------

module "d4science-networking-spoke-prod-pga" {
  source     = "../assets/modules-custom/pga"
  project_id = module.d4science-networking-spoke-prod-project.project_id
  name       = "d4science-prod-pga"
  domains = {
    artifact = true
  }
  config = {
    private = true
  }
  networks = [
    module.d4science-networking-prod-vpc.self_link
  ]
}