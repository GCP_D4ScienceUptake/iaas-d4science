variable "outputs_location" {
  description = "Assets path location relative to the module"
  type        = string
  default     = "../assets"
}

variable "gke_subnet_id" {
  description = "Id of the subnet where gke cluster must reside"
  type        = string
  default     = ""
}

variable "gke_region" {
  description = "Region where gke cluster is located"
  type        = string
  default     = ""
}

variable "gke_master_auth_networks" {
  description = "Networks which are allowed to access gke control plane"
  type        = map(string)
  default     = {}
}