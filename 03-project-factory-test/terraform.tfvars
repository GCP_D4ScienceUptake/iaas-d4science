gke_region    = "europe-west8"
gke_subnet_id = "europe-west8/d4science-test-ew8-vre-sub"
gke_master_auth_networks = {
  "cnr-d4science-1" = "146.48.28.0/22",
  "cnr-d4science-2" = "146.48.122.0/23",
  "cnr-d4science-3" = "145.90.225.224/27",
  # "cnr-d4science-4" = "2001:610:450:80::/64",
  "reply-vpn-1"          = "91.218.224.5/32",
  "reply-vpn-2"          = "91.218.224.15/32",
  "reply-vpn-3"          = "91.218.226.5/32",
  "reply-vpn-4"          = "91.218.226.15/32",
  "budget-cf-vpc-access" = "10.254.66.0/28"
}