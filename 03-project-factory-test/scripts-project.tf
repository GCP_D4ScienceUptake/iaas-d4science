module "d4science-test-budget-script-project" {
  source = "../assets/modules-fabric/v26/project"

  name            = "d4science-test-script-prj"
  prefix          = local.prefix
  billing_account = local.billing_account_id
  parent          = local.folders.test.id
  labels          = local.labels

  auto_create_network = false
  project_create      = true

  services = [
    "cloudresourcemanager.googleapis.com", #required for SA impersonification
    "iam.googleapis.com",                  #required for IAM and SA impersonification
    "cloudfunctions.googleapis.com",       #required for Cloud Functions
    "pubsub.googleapis.com",               #required for PubSub
    "billingbudgets.googleapis.com",       #required for Billing alerting,
    "run.googleapis.com",                  #required for Cloud Functions 2nd gen
    "cloudbuild.googleapis.com",           #required for Cloud Functions 2nd gen
    "eventarc.googleapis.com",             #required for Cloud Functions 2nd gen
    "vpcaccess.googleapis.com",            #required for Serverless VPC access
  ]

  iam = {
    "roles/owner" = [
      local.service_accounts.project_factory_test,
    ]
  }

  group_iam = {
    "foundationreply@d4science.org" = [
      "roles/owner"
    ]
  }
}

module "d4science-test-budget-alert-topic" {
  source     = "../assets/modules-fabric/v26/pubsub"
  project_id = module.d4science-test-budget-script-project.project_id
  name       = "d4science-test-budget-alerting-topic"
}

# module "d4science-test-billing-budget-overall" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Test - Overall budget"
#   amount          = 1000
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.75, 1.0]
#     forecasted = []
#   }

#   projects = [
#     "projects/${local.networking.spoke-test-project.number}",
#     "projects/${module.d4science-test-budget-script-project.number}",
#     "projects/${module.d4science-test-vre-project.number}"
#   ]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
# }

# module "d4science-test-billing-budget-jupyter-hub" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Test - GKE - JupyterHub namespace budget"
#   amount          = 100
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.75, 1.0]
#     forecasted = []
#   }
#   labels = {
#     "k8s-namespace" = "jupyter-hub"
#   }
#   resource_ancestors    = [local.folders.test.id]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
#   pubsub_topic          = module.d4science-test-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
# }

# module "d4science-test-billing-budget-blue-cloud" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Test - GKE - BlueCloud namespace budget"
#   amount          = 100
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.75, 1.0]
#     forecasted = []
#   }
#   labels = {
#     "k8s-namespace" = "blue-cloud"
#   }
#   resource_ancestors    = [local.folders.test.id]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
#   pubsub_topic          = module.d4science-test-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
# }

# module "d4science-test-billing-budget-i-marine" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Test - GKE - iMarine namespace budget"
#   amount          = 100
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.75, 1.0]
#     forecasted = []
#   }
#   labels = {
#     "k8s-namespace" = "i-marine"
#   }
#   resource_ancestors    = [local.folders.test.id]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
#   pubsub_topic          = module.d4science-test-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
# }

# module "d4science-test-billing-budget-so-big-data" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Test - GKE - SoBigData namespace budget"
#   amount          = 100
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.75, 1.0]
#     forecasted = []
#   }
#   labels = {
#     "k8s-namespace" = "so-big-data"
#   }
#   resource_ancestors    = [local.folders.test.id]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
#   pubsub_topic          = module.d4science-test-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
# }

# module "d4science-test-billing-budget-open-community" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Test - GKE - OpenCommunity namespace budget"
#   amount          = 100
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.75, 1.0]
#     forecasted = []
#   }
#   labels = {
#     "k8s-namespace" = "open-community"
#   }
#   resource_ancestors    = [local.folders.test.id]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
#   pubsub_topic          = module.d4science-test-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
# }

module "d4science-test-budget-script-sa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.d4science-test-budget-script-project.project_id
  name       = "d4science-test-budget-cf-sa"

  iam = {
    "roles/iam.serviceAccountTokenCreator" = ["group:foundationreply@d4science.org"]
    #Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  }

  iam_billing_roles = {
    "${local.billing_account_id}" = ["roles/billing.viewer"]
  }
}

resource "google_vpc_access_connector" "d4science-test-budget-script-vpc-connector" {
  project = module.d4science-test-budget-script-project.project_id
  name    = "d4science-test-cf-vpc-con"
  region  = var.gke_region
  subnet {
    name       = "d4science-test-ew8-vpc-con-sub"
    project_id = local.networking.spoke-test-project.project_id
  }
}