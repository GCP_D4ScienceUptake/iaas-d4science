module "d4science-test-vre-gke-cluster" {
  source     = "../assets/modules-fabric/v26/gke-cluster-standard"
  project_id = module.d4science-test-vre-project.project_id
  name       = "d4science-test-vre-gke-cluster"
  location   = var.gke_region
  vpc_config = {
    network    = local.networking.spoke-test-project.network
    subnetwork = local.networking.spoke-test-project.subnets[var.gke_subnet_id].self_link
    secondary_range_names = {
      pods     = "pods"
      services = "services"
    }
    master_authorized_ranges = var.gke_master_auth_networks
    master_ipv4_cidr_block   = "10.249.0.64/28"
  }
  private_cluster_config = {
    enable_private_endpoint = false
    master_global_access    = false
  }
  enable_features = {
    dataplane_v2      = true
    workload_identity = true
    cost_management   = true
  }
  enable_addons = {
    gce_persistent_disk_csi_driver = true
    gcp_filestore_csi_driver       = true
    horizontal_pod_autoscaling     = true
    http_load_balancing            = true
  }
  logging_config = {
    enable_workloads_logs          = true
    enable_api_server_logs         = true
    enable_scheduler_logs          = true
    enable_controller_manager_logs = true
  }
  monitoring_config = {
    enable_api_server_metrics         = true
    enable_controller_manager_metrics = true
    enable_scheduler_metrics          = true
    enable_daemonset_metrics          = true
    enable_deployment_metrics         = true
    enable_hpa_metrics                = true
    enable_pod_metrics                = true
    enable_statefulset_metrics        = true
    enable_storage_metrics            = true
  }
  labels = {
    environment = "test"
  }
}

module "d4science-test-vre-gke-nodepool" {
  source       = "../assets/modules-fabric/v26/gke-nodepool"
  project_id   = module.d4science-test-vre-project.project_id
  cluster_name = module.d4science-test-vre-gke-cluster.name
  location     = var.gke_region
  name         = "d4science-test-vre-gke-nodepool"
  service_account = {
    create       = true
    email        = "d4science-test-vre-nodepoolsa"
    oauth_scopes = ["https://www.googleapis.com/auth/cloud-platform"]
  }
  node_config = {
    machine_type = "e2-medium"
    gcfs = true
  }
  nodepool_config = {
    autoscaling = {
      max_node_count = 2
      min_node_count = 0
    }
    management = {
      auto_repair  = true
      auto_upgrade = true
    }
  }
}

module "d4science_test_artifact_registry" {
  source     = "../assets/modules-fabric/v26/artifact-registry"
  project_id = module.d4science-test-vre-project.project_id
  location   = var.gke_region
  name       = "d4science-test-images"
  iam = {
    "roles/artifactregistry.reader" = [module.d4science-test-vre-gke-nodepool.service_account_iam_email]
  }
}