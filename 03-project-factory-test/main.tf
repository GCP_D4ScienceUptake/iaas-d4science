locals {
  organization_vars = jsondecode(file("../assets/tfvars/00-organization.auto.tfvars.json"))
  networking_vars   = jsondecode(file("../assets/tfvars/01-networking.auto.tfvars.json"))
}
locals {

  billing_account_id = local.organization_vars.billing_account_id
  organization       = local.organization_vars.organization
  prefix             = local.organization_vars.prefix
  labels             = local.organization_vars.labels
  groups             = local.organization_vars.groups

  # Global variables

  seed-project = {
    project_id     = local.organization_vars.seed-project.project_id
    project_number = local.organization_vars.seed-project.project_number
  }

  service_accounts = {
    networking           = local.organization_vars.service_accounts.networking
    security             = local.organization_vars.service_accounts.security
    project_factory_test = local.organization_vars.service_accounts.project_factory_test
  }

  networking = {
    spoke-test-project = local.networking_vars.spoke-test
  }

  monitoring = {
    bucket-sink = local.organization_vars.monitoring.bucket-sink-test
    project_id  = local.organization_vars.monitoring.project_id
    channels    = local.organization_vars.monitoring.channels
  }

  # Folders
  folders = {
    test = {
      id   = local.organization_vars.folders.test.id
      name = local.organization_vars.folders.test.name
    }
  }
}

