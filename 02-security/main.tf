locals {
  networking_vars = jsondecode(file("../assets/tfvars/01-networking.auto.tfvars.json"))
}
locals {
  spoke-test = {
    project_id = local.networking_vars.spoke-test.project_id
    network    = local.networking_vars.spoke-test.network
  }
  spoke-prod = {
    project_id = local.networking_vars.spoke-prod.project_id
    network    = local.networking_vars.spoke-prod.network
  }
}