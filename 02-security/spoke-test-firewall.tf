#----------------------------------
# Spoke TEST Firewall
#----------------------------------
module "d4science-security-spoke-test-firewall" {
  source     = "../assets/modules-fabric/v26/net-vpc-firewall"
  project_id = local.spoke-test.project_id
  network    = local.spoke-test.network

  default_rules_config = {
    disabled = true
  }

  factories_config = {
    rules_folder  = "firewall/rules/spoke-test"
    cidr_tpl_file = "firewall/cidrs.yaml"
  }
}