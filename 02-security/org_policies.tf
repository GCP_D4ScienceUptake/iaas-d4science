##-------------------------------------------------------------------------------
## 02-Security - Organization - ORG POLICIES
##-------------------------------------------------------------------------------
module "org" {
  source                 = "../assets/modules-fabric/v26/organization"
  organization_id        = "organizations/${var.organization.id}"
  org_policies_data_path = "firewall/rules/org-policies"
}