#----------------------------------
# Spoke PROD Firewall
#----------------------------------
module "d4science-security-spoke-prod-firewall" {
  source     = "../assets/modules-fabric/v26/net-vpc-firewall"
  project_id = local.spoke-prod.project_id
  network    = local.spoke-prod.network

  default_rules_config = {
    disabled = true
  }

  factories_config = {
    rules_folder  = "firewall/rules/spoke-prod"
    cidr_tpl_file = "firewall/cidrs.yaml"
  }
}