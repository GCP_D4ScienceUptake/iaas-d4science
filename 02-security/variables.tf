variable "organization" {
  description = "Organization details."
  type = object({
    domain = string
    id     = number
  })
}