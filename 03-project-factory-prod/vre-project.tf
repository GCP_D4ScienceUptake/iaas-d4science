module "d4science-prod-vre-project" {
  source = "../assets/modules-fabric/v26/project"

  name            = "d4science-prod-vre-prj"
  prefix          = local.prefix
  billing_account = local.billing_account_id
  parent          = local.folders.prod.id
  labels          = local.labels

  auto_create_network = false
  project_create      = true

  services = [
    "cloudresourcemanager.googleapis.com", #required for SA impersonification
    "iam.googleapis.com",                  #required for IAM and SA impersonification
    "container.googleapis.com",            #required for GKE
    "artifactregistry.googleapis.com",     #required for Artifact registry
    "pubsub.googleapis.com",               #required for PubSub
    "billingbudgets.googleapis.com",       #required for Billing alerting 
    "certificatemanager.googleapis.com",   #required for certificates
    "secretmanager.googleapis.com",        #required for secrets
    "logging.googleapis.com",              #required for logging
    "file.googleapis.com",                 #required for Filestore
    "servicenetworking.googleapis.com",
    "containerfilesystem.googleapis.com" #required for image streaming in GKE
  ]

  iam = {
    "roles/owner" = [
      local.service_accounts.project_factory_prod,
    ]
    "roles/container.developer" = ["serviceAccount:${module.d4science-prod-budget-script-sa.email}"]
  }

  group_iam = {
    "foundationreply@d4science.org" = [
      "roles/owner"
    ]
  }

  logging_sinks = {
    gke = {
      destination   = local.monitoring.bucket-sink.id
      filter        = "resource.type=\"k8s_cluster\""
      type          = "logging"
      unique_writer = true
    }
  }
}

module "d4science-prod-vre-addresses" {
  source           = "../assets/modules-fabric/v26/net-address"
  project_id       = module.d4science-prod-vre-project.project_id
  global_addresses = ["d4science-prod-vre-address-ext"]
}

module "d4science-prod-vre-ssl-secret" {
  source     = "../assets/modules-fabric/v26/secret-manager"
  project_id = module.d4science-prod-vre-project.project_id
  secrets = {
    # Must be uploaded manually
    d4science-prod-vre-ssl-private-key = ["europe-west4"],
    d4science-prod-vre-ssl-public-cert = ["europe-west4"]
  }
}

resource "google_compute_ssl_policy" "d4science-prod-vre-ssl-policy-1_2-modern" {
  project         = module.d4science-prod-vre-project.project_id
  name            = "d4science-prod-ssl-policy-1-2-modern"
  profile         = "MODERN"
  min_tls_version = "TLS_1_2"
}

resource "google_compute_security_policy" "d4science-prod-vre-armor-policy" {
  project = module.d4science-prod-vre-project.project_id
  name    = "d4science-prod-vre-armor-policy"
  adaptive_protection_config {
    layer_7_ddos_defense_config {
      enable = true
    }
  }

  rule {
    action      = "deny(403)"
    description = "Block RU"
    priority    = "1000"
    match {
      expr {
        expression = "origin.region_code == 'RU'"
      }
    }
  }

  rule {
    action   = "allow"
    priority = "2147483647"
    match {
      versioned_expr = "SRC_IPS_V1"
      config {
        src_ip_ranges = ["*"]
      }
    }
    description = "default rule"
  }
}