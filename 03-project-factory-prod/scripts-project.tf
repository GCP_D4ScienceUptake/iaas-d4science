module "d4science-prod-budget-script-project" {
  source = "../assets/modules-fabric/v26/project"

  name            = "d4science-prod-script-prj"
  prefix          = local.prefix
  billing_account = local.billing_account_id
  parent          = local.folders.prod.id
  labels          = local.labels

  auto_create_network = false
  project_create      = true

  services = [
    "cloudresourcemanager.googleapis.com", #required for SA impersonification
    "iam.googleapis.com",                  #required for IAM and SA impersonification
    "cloudfunctions.googleapis.com",       #required for Cloud Functions
    "pubsub.googleapis.com",               #required for PubSub
    "billingbudgets.googleapis.com",       #required for Billing alerting,
    "run.googleapis.com",                  #required for Cloud Functions 2nd gen
    "cloudbuild.googleapis.com",           #required for Cloud Functions 2nd gen
    "eventarc.googleapis.com",             #required for Cloud Functions 2nd gen
    "vpcaccess.googleapis.com",            #required for Serverless VPC access
  ]

  iam = {
    "roles/owner" = [
      local.service_accounts.project_factory_prod,
    ]
  }

  group_iam = {
    "foundationreply@d4science.org" = [
      "roles/owner"
    ]
  }
}

module "d4science-prod-budget-alert-topic" {
  source     = "../assets/modules-fabric/v26/pubsub"
  project_id = module.d4science-prod-budget-script-project.project_id
  name       = "d4science-prod-budget-alerting-topic"
}

module "d4science-billing-budget-overall-year-critical" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Critical - Overall yearly budget"
  amount          = 442500
  currency_code   = "EUR"
  thresholds = {
    current    = [0.95, 0.97]
    forecasted = []
  }
  calendar_period       = "YEAR"
  resource_ancestors    = ["organizations/${local.organization.id}"]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-billing-budget-overall-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Overall yearly budget"
  amount          = 442500
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 0.9]
    forecasted = []
  }
  calendar_period       = "YEAR"
  resource_ancestors    = ["organizations/${local.organization.id}"]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-billing-budget-overall-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Overall monthly budget"
  amount          = 36875
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  calendar_period       = "MONTH"
  resource_ancestors    = ["organizations/${local.organization.id}"]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-blue-cloud-total-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - BlueCloud cumulative yearly budget"
  amount          = 168000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-namespace" = "blue-cloud"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
  pubsub_topic          = module.d4science-prod-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
}

module "d4science-prod-billing-budget-i-marine-total-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - iMarine cumulative yearly budget"
  amount          = 40500
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-namespace" = "i-marine"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
  pubsub_topic          = module.d4science-prod-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
}

module "d4science-prod-billing-budget-so-big-data-total-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - SoBigData cumulative yearly budget"
  amount          = 147000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-namespace" = "so-big-data"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
  pubsub_topic          = module.d4science-prod-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
}

module "d4science-prod-billing-budget-open-community-total-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - OpenCommunity cumulative yearly budget"
  amount          = 87000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-namespace" = "open-community"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
  pubsub_topic          = module.d4science-prod-budget-alert-topic.id # Attention: this cannot be done without temporarily deactivating the org policy iam.allowedPolicyMemberDomains
}

module "d4science-prod-billing-budget-blue-cloud-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - BlueCloud namespace yearly budget"
  amount          = 117000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "blue-cloud"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-i-marine-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - iMarine namespace yearly budget"
  amount          = 22500
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "i-marine"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-so-big-data-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - SoBigData namespace yearly budget"
  amount          = 117000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "so-big-data"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-open-community-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - OpenCommunity namespace yearly budget"
  amount          = 87000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "open-community"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-blue-cloud-datathon-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - BlueCloudDatathon namespace yearly budget"
  amount          = 51000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "blue-cloud-datathon"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-i-marine-datathon-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - iMarineDatathon namespace yearly budget"
  amount          = 18000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "i-marine-datathon"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-so-big-data-datathon-year" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - SoBigDataDatathon namespace yearly budget"
  amount          = 30000
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "so-big-data-datathon"
  }
  calendar_period       = "YEAR"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

# module "d4science-prod-billing-budget-open-community-year" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Prod - GKE - OpenCommunityDatathon namespace yearly budget"
#   amount          = 0
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.8, 1.0]
#     forecasted = []
#   }
#   labels = {
#     "k8s-label/d4science-namespace" = "open-community-datathon"
#   }
#   calendar_period = "YEAR"
#   resource_ancestors    = [local.folders.prod.id]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
# }

module "d4science-prod-billing-budget-blue-cloud-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - BlueCloud namespace monthly budget"
  amount          = 9750
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "blue-cloud"
  }
  calendar_period       = "MONTH"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-i-marine-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - iMarine namespace monthly budget"
  amount          = 1875
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "i-marine"
  }
  calendar_period       = "MONTH"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-so-big-data-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - SoBigData namespace monthly budget"
  amount          = 9750
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "so-big-data"
  }
  calendar_period       = "MONTH"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-open-community-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - OpenCommunity namespace monthly budget"
  amount          = 7250
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "open-community"
  }
  calendar_period       = "MONTH"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-blue-cloud-datathon-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - BlueCloudDatathon namespace monthly budget"
  amount          = 4250
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "blue-cloud-datathon"
  }
  calendar_period       = "MONTH"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-i-marine-datathon-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - iMarineDatathon namespace monthly budget"
  amount          = 1500
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "i-marine-datathon"
  }
  calendar_period       = "MONTH"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

module "d4science-prod-billing-budget-so-big-data-datathon-month" {
  source          = "../assets/modules-custom/billing-budget"
  billing_account = local.billing_account_id
  name            = "Prod - GKE - SoBigDataDatathon namespace monthly budget"
  amount          = 2500
  currency_code   = "EUR"
  thresholds = {
    current    = [0.5, 0.8, 1.0]
    forecasted = []
  }
  labels = {
    "k8s-label/d4science-namespace" = "so-big-data-datathon"
  }
  calendar_period       = "MONTH"
  resource_ancestors    = [local.folders.prod.id]
  notification_channels = [local.monitoring.channels["budget-alerting"]]
}

# module "d4science-prod-billing-budget-open-community-datathon-month" {
#   source          = "../assets/modules-custom/billing-budget"
#   billing_account = local.billing_account_id
#   name            = "Prod - GKE - OpenCommunityDatathon namespace monthly budget"
#   amount          = 0
#   currency_code   = "EUR"
#   thresholds = {
#     current    = [0.5, 0.8, 1.0]
#     forecasted = []
#   }
#   labels = {
#     "k8s-label/d4science-namespace" = "open-community-datathon"
#   }
#   calendar_period = "MONTH"
#   resource_ancestors    = [local.folders.prod.id]
#   notification_channels = [local.monitoring.channels["budget-alerting"]]
# }

module "d4science-prod-budget-script-sa" {
  source = "../assets/modules-fabric/v26/iam-service-account"

  project_id = module.d4science-prod-budget-script-project.project_id
  name       = "d4science-prod-budget-cf-sa"

  iam = {
    "roles/iam.serviceAccountTokenCreator" = ["group:foundationreply@d4science.org"]
    #Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  }

  iam_billing_roles = {
    "${local.billing_account_id}" = ["roles/billing.viewer"]
  }
}

resource "google_vpc_access_connector" "d4science-prod-budget-script-vpc-connector" {
  project = module.d4science-prod-budget-script-project.project_id
  name    = "d4science-prod-cf-vpc-con"
  region  = var.gke_region
  subnet {
    name       = "d4science-prod-ew4-vpc-con-sub"
    project_id = local.networking.spoke-prod-project.project_id
  }
}