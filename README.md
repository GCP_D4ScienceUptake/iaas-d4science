gcloud auth application-default login

## D4Science Foundation
This repository contains all the folder structure for D4Science foundation to support VRE applications.

### PRJ Details
- Billing account: `018258-BCA804-E9D4C6`
- Tenant:
    - `d4science`

## Folder structure
```
.
├── 00-organization
├── 01-networking
│   └── data
│       └── subnets
├── 02-security
│   └── firewall
│       └── rules
│           ├── 00-spoke-test
│           ├── 01-spoke-prod
├── 03-monitoring
│   └── data
├── 04-project-factory-test
├── 04-project-factory-prod
└── assets
    ├── providers
    └── tfvars
```

## Naming Convention
The naming convention for this foundation project is the following one:
`<tenant>-<env>-<name>-prj`

- tenant: `d4science`
- env:
    - test
    - prod
- name: project name

### Resources
The naming convention to use for the resources of this project is:

`<tenant>-<env>-<location (optional)-<app. initiative (optional)>-<unique id>-<res. short name>`

#### Resource name examples
- Service Account:  `d4science-dev-foundation-tfnet-sa`
- VPC: `d4science-dev-glb-spoke-vpc`  (location glb = global)
- Subnet: `d4science-dev-ew8-01-sub`  (unique id = 01 used as incremental counter)
- Bucket: `d4science-org-ew8-foundation-tforg-bkt`
- peering: `d4science-prod-hubspoke-peer`

### Firewall Rule
The naming convention to use for the firewall rules of this project is:

`<tenant>-<env>-glb-<app. initiative>-<ing/egr>-<alw/den>-<unique id>-<res. short name>`

#### FW rule example
- FW rule: `d4science-test-glb-paytas-ing-alw-gke-fwr`

### General considerations
- Avoid fields with "-"
- unique id can be:
    - numbers (e.g. 01)
    - letters (e.g. abc)
    - both (e.g. abc01)

### SHORT NAMES
#### Resources
- SA: service account
- BKT: bucket
- PRJ: project
- SUB: subnet
- VPC: vpc
- FWR: Firewall